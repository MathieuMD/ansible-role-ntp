ansible-role-ntp
================

Install and configure ntp.

Role Variables
--------------

- `ntp_config_server` : list of ntp servers to query.

Example Playbook
----------------

1) Install ntp and set the default settings:

```yaml
- hosts: all
  roles:
  - ntp
```

2) Install ntp and set some custom servers:

```yaml
- hosts: all
  roles:
  - role: ntp
    #ntp_config_server: [ 'europe.pool.ntp.org iburst', 'north-america.pool.ntp.org iburst' ]
    ntp_config_server: 
    - 2.ubuntu.pool.ntp.org iburst
    - 1.ubuntu.pool.ntp.org iburst
```

License
-------

BSD

Author Information
------------------

Based on ntp roles by [Benno Joy](https://github.com/bennojoy/ntp) and [René Moser](https://github.com/resmo/ansible-role-ntp).
